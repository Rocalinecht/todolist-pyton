from django.shortcuts import render
import pyrebase
from django.contrib import auth
# # Import database module.
# from firebase_admin import db

# # Get a database reference to our blog.
# ref = db.reference('/task')
# print(ref)
# 	firebase.database()

config = {
    'apiKey': "AIzaSyBnlf5lR91URPOmij4QxnoU7k0zKXsVLr8",
    'authDomain': "todolist-python-6ea25.firebaseapp.com",
    'databaseURL': "https://todolist-python-6ea25.firebaseio.com",
    'projectId': "todolist-python-6ea25",
    'storageBucket': "todolist-python-6ea25.appspot.com",
    'messagingSenderId': "859917198069",
}


firebase = pyrebase.initialize_app(config)

authe = firebase.auth()

db = firebase.database()




# Page identification 
def signIn(request):
    return render(request, "signIn.html")

def postsign(request):
    email=request.POST.get('email')
    passw = request.POST.get("pass")

    try:
        user = authe.sign_in_with_email_and_password(email,passw)

    except:
        message = "Identification incorrect"
        return render(request,"signIn.html",{"msg":message})

    # print(user['idToken'])
    session_id = user['idToken']
    request.session['session_user'] = user
    
    return render(request, "welcome.html",{"e":email})


# Deconnexion 
def logout(request):
    auth.logout(request)
    return render(request, 'signIn.html')


# Ajouter une tache 
def postTask (request) :

    data = {
        'tasks' : request.POST.get('tasks')
    }

    user = request.session["session_user"]

    authe.refresh(user['refreshToken'])

    results = db.child("tasks").push(data, user["idToken"])

    tasks = getFirebaseMessages()

    return render(request, "welcome.html", {"e":user["email"],"t":tasks})

# Afficher la liste des taches
def getFirebaseMessages():

    list_tasks = {}
    tasks = db.child("tasks").get()

    for t in tasks.each():
        list_tasks[t.key()] = t.val()["tasks"]

    return list_tasks